[![pipeline status](https://gitlab.com/smirngreg/pyndes/badges/master/pipeline.svg)](https://gitlab.com/smirngreg/pyndes/commits/master)
[![coverage report](https://gitlab.com/smirngreg/pyndes/badges/master/coverage.svg)](https://gitlab.com/smirngreg/pyndes/commits/master)

This repository contains python and other code that may be used in a large number of projects of the author, Grigorii Smirnov-Pinchukov.

You better don't use it without asking the author (smirnov@mpia.de), mostly because this code has even less warranty than you could expect.

Read documentation [here](https://smirngreg.gitlab.io/pyndes/index.html).
