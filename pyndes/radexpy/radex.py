#!/usr/bin/env python3

"""
This module contains a python interface to RADEX code.
"""

import subprocess
import io
import tempfile
import re
import os
import shutil

from astropy import units as u
import astropy.io.ascii
import astropy.table.table

from pyndes.radexpy.engine import Radex

RADEX_DOCKER = Radex.RADEX_DOCKER
PATH_RADEX = Radex.PATH_RADEX
RADEX_IN_PATH = Radex.RADEX_IN_PATH



def get_output_table(
        molecular_data_file="co.dat",
        minimum_frequency=10 * u.GHz,
        maximum_frequency=1000 * u.GHz,
        kinetic_temperature=20 * u.K,
        background_temperature=2.73 * u.K,
        molecular_column_density=1e13 * u.cm ** (-2),
        collision_partners=(("H2", 1e4 * u.cm ** (-3)),),
        line_width=1. * u.km / u.s,
        executable=PATH_RADEX,
        temporary_file_kwargs=None,
):
    """
    This function runs RADEX to calculate line flux.

    Make sure to install RADEX first! This function just runs an existing instance of RADEX.
    Provide all Einstein-coefficients data from LAMDA database to RADEX datapath folder.
    Args:
        molecular_data_file: file from LAMDA for the molecule
        minimum_frequency: minimum frequency to search lines, in astropy.units
        maximum_frequency: maximum frequency to search lines, in astropy.units
        kinetic_temperature: temperature of the gas, in astropy.units
        background_temperature: temperature of the background radiation, in astropy.units
        molecular_column_density: column density of molecule, in astropy.units,
            the column density must be between 1e5 and 1e25 cm^-2
        collision_partners: dictionary containing collision partners and their volume density in astropy.units,
            the volume density must be between 1e-3 and 1e13 cm^-3
        line_width: velocity dispersion in the cloud, in astropy.units velocity
        executable: path to RADEX executable, if not available in PATH
        temporary_file_kwargs: kwargs to be passed to tempfile.NamedTemporaryFile

    Returns: RADEX output in astropy.table format with astropy.units where applicable

    Raises: ValueError, if the collision partner density or molecular column density is out of bounds
    """
    rdx = Radex(executable=executable)
    output_tbl = rdx.call_single(
        molecular_data_file=molecular_data_file,
        minimum_frequency=minimum_frequency,
        maximum_frequency=maximum_frequency,
        kinetic_temperature=kinetic_temperature,
        background_temperature=background_temperature,
        molecular_column_density=molecular_column_density,
        collision_partners=collision_partners,
        line_width=line_width,
        temporary_file_kwargs=temporary_file_kwargs,
    )
    return output_tbl


if __name__ == '__main__':
    get_output_table(executable="/home/smirnov/astrowork/disk/Radex/bin/radex")
