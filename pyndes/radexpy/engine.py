"""Runner engine for radex"""
import shutil
import subprocess
import io
import os
import tempfile
import re
import json

import numpy as np

from astropy import units as u
import astropy.io, astropy.table


class Radex:
    RADEX_DOCKER = 'registry.gitlab.com/smirngreg/radexdocker:latest'
    PATH_RADEX = 'radex'
    RADEX_IN_PATH = shutil.which(PATH_RADEX) is not None

    if not RADEX_IN_PATH:
        if shutil.which('docker'):
            subprocess.run(['docker', 'pull', RADEX_DOCKER])
            PATH_RADEX = f'docker run -v {os.getcwd()}:/Radex/out -i {RADEX_DOCKER} radex'
            RADEX_IN_PATH = True
    else:
        if subprocess.run(["wsl", "which", "radex"], capture_output=True).stdout is not None:
            PATH_RADEX = "wsl radex"
            RADEX_IN_PATH = True

    def __init__(
            self,
            executable=PATH_RADEX
    ):
        self.executable = executable

    def inputs_to_str(
            self,
            molecular_data_file,
            filename,
            minimum_frequency,
            maximum_frequency,
            kinetic_temperature,
            background_temperature,
            molecular_column_density,
            collision_partners,
            line_width,
            append_to=None,
            end=True,
    ):
        if not (1e5 <= molecular_column_density * u.cm ** 2 <= 1e25):
            raise ValueError(f"Column density for {molecular_data_file} is too low ({molecular_column_density})!"
                             f"It must be between 1e5 and 1e25 cm^-2")

        collision_partners_dict = dict(collision_partners)
        for partner in collision_partners_dict.keys():
            if not (1e-3 <= collision_partners_dict[partner] * u.cm ** 3 <= 1e13):
                raise ValueError(f"Collision density for {partner} is too low ({collision_partners_dict[partner]})!"
                                 f"It must be between 1e-3 and 1e13 cm^-3")

        radex_input = io.StringIO()
        if append_to:
            print(append_to, file=radex_input, end="")

        print(molecular_data_file, file=radex_input)
        print(os.path.relpath(filename), file=radex_input)
        print(f"{minimum_frequency.to(u.GHz).value} {maximum_frequency.to(u.GHz).value} ", file=radex_input)
        print(f"{kinetic_temperature.to(u.K).value}", file=radex_input)
        print(f"{len(collision_partners_dict)}", file=radex_input)
        for partner in collision_partners_dict.keys():
            print(partner, file=radex_input)
            print(f"{collision_partners_dict[partner].to(u.cm ** (-3)).value}", file=radex_input)
        print(f"{background_temperature.to(u.K).value}", file=radex_input)
        print(f"{molecular_column_density.to(u.cm ** (-2)).value}", file=radex_input)
        print(f"{line_width.to(u.km / u.s).value}", file=radex_input)
        print(f"{1 if end else 0}", file=radex_input)

        return radex_input.getvalue()

    def call_single(
            self,
            molecular_data_file: str = "co.dat",
            minimum_frequency: u.Quantity = 10 * u.GHz,
            maximum_frequency: u.Quantity = 1000 * u.GHz,
            kinetic_temperature: u.Quantity = 20 * u.K,
            background_temperature: u.Quantity = 2.73 * u.K,
            molecular_column_density: u.Quantity = 1e13 * u.cm ** (-2),
            collision_partners=(("H2", 1e4 * u.cm ** (-3)),),
            line_width: u.Quantity = 1. * u.km / u.s,
            temporary_file_kwargs=(("dir", "."),),
    ):
        """
        This method runs RADEX to calculate line flux.

        Make sure to install RADEX first! This function just runs an existing instance of RADEX.
        Provide all Einstein-coefficients data from LAMDA database to RADEX datapath folder.
        Args:
            molecular_data_file: file from LAMDA for the molecule
            minimum_frequency: minimum frequency to search lines, in astropy.units
            maximum_frequency: maximum frequency to search lines, in astropy.units
            kinetic_temperature: temperature of the gas, in astropy.units
            background_temperature: temperature of the background radiation, in astropy.units
            molecular_column_density: column density of molecule, in astropy.units,
                the column density must be between 1e5 and 1e25 cm^-2
            collision_partners: dictionary containing collision partners and their volume density in astropy.units,
                the volume density must be between 1e-3 and 1e13 cm^-3
            line_width: velocity dispersion in the cloud, in astropy.units velocity
            executable: path to RADEX executable, if not available in PATH
            temporary_file_kwargs: kwargs to be passed to tempfile.NamedTemporaryFile

        Returns: RADEX output in astropy.table format with astropy.units where applicable

        Raises: ValueError, if the collision partner density or molecular column density is out of bounds
        """

        temporary_file_kwargs = dict(temporary_file_kwargs)
        with tempfile.NamedTemporaryFile(**temporary_file_kwargs) as temporary_output_file:
            radex_input = self.inputs_to_str(
                molecular_data_file=molecular_data_file,
                filename=temporary_output_file.name,
                minimum_frequency=minimum_frequency,
                maximum_frequency=maximum_frequency,
                kinetic_temperature=kinetic_temperature,
                background_temperature=background_temperature,
                molecular_column_density=molecular_column_density,
                collision_partners=collision_partners,
                line_width=line_width,
            )

            proc = subprocess.run(self.executable, input=radex_input, text=True, capture_output=True)

            output = temporary_output_file.readlines()
            line_with_transitions = [
                outputline.decode('utf-8') if bool(re.search(r"\d+\s*--\s*\d+", str(outputline))) else ""
                for outputline in output
            ]
        temp_tbl = astropy.io.ascii.read(line_with_transitions)
        output_tbl = self.add_labels_and_units(temp_tbl)
        return output_tbl

    def call_multiple(
            self,
            molecular_data_file: str = "co.dat",
            minimum_frequency: u.Quantity = 10 * u.GHz,
            maximum_frequency: u.Quantity = 1000 * u.GHz,
            kinetic_temperature: u.Quantity = 20 * u.K,
            background_temperature: u.Quantity = 2.73 * u.K,
            molecular_column_density: u.Quantity = 1e13 * u.cm ** (-2),
            collision_partners=(("H2", 1e4 * u.cm ** (-3)),),
            line_width: u.Quantity = 1. * u.km / u.s,
            temporary_file_kwargs=(("dir", "."),),
    ):
        """
        This method runs RADEX to calculate line flux.

        Make sure to install RADEX first! This function just runs an existing instance of RADEX.
        Provide all Einstein-coefficients data from LAMDA database to RADEX datapath folder.
        Args:
            molecular_data_file: file from LAMDA for the molecule
            minimum_frequency: minimum frequency to search lines, in astropy.units
            maximum_frequency: maximum frequency to search lines, in astropy.units
            kinetic_temperature: temperature of the gas, in astropy.units
            background_temperature: temperature of the background radiation, in astropy.units
            molecular_column_density: column density of molecule, in astropy.units,
                the column density must be between 1e5 and 1e25 cm^-2
            collision_partners: dictionary containing collision partners and their volume density in astropy.units,
                the volume density must be between 1e-3 and 1e13 cm^-3
            line_width: velocity dispersion in the cloud, in astropy.units velocity
            executable: path to RADEX executable, if not available in PATH
            temporary_file_kwargs: kwargs to be passed to tempfile.NamedTemporaryFile

        Returns: RADEX output in astropy.table format with astropy.units where applicable

        Raises: ValueError, if the collision partner density or molecular column density is out of bounds
        """

        temporary_file_kwargs = dict(temporary_file_kwargs)
        collision_partners = dict(collision_partners)

        input_table = astropy.table.QTable()
        input_table["Molecular Column Density"] = molecular_column_density
        input_table["Kinetic Temperature"] = kinetic_temperature
        input_table["Molecular data file"] = molecular_data_file
        input_table["Minimum Frequency"] = minimum_frequency
        input_table["Maximum Frequency"] = maximum_frequency
        input_table["Background Temperature"] = background_temperature
        input_table["Collision Partners"] = np.array(collision_partners)

        output_list = [None] * len(input_table)
        with tempfile.NamedTemporaryFile(**temporary_file_kwargs) as temporary_output_file:
            radex_input = ""
            for i, entry in enumerate(input_table):
                try:
                    radex_input = self.inputs_to_str(
                        molecular_data_file=entry["Molecular data file"],
                        filename=temporary_output_file.name,
                        minimum_frequency=entry["Minimum Frequency"],
                        maximum_frequency=entry["Maximum Frequency"],
                        kinetic_temperature=entry["Kinetic Temperature"],
                        background_temperature=entry["Background Temperature"],
                        molecular_column_density=entry["Molecular Column Density"],
                        collision_partners=entry["Collision Partners"],
                        line_width=line_width,
                        end=(i != len(input_table) - 1),
                        append_to=radex_input
                    )
                except ValueError:
                    output_list[i] = np.nan

            proc = subprocess.run(self.executable, input=radex_input, text=True, capture_output=True)
            output = temporary_output_file.readlines()
            line_with_transitions = [
                outputline.decode('utf-8') if bool(re.search(r"\d+\s*--\s*\d+", str(outputline))) else ""
                for outputline in output
            ]
        temp_tbl = astropy.io.ascii.read(line_with_transitions)
        output_tbl = self.add_labels_and_units(temp_tbl)
        return output_tbl

    @staticmethod
    def parse_output(output):
        output = [outputline.decode('utf-8') for outputline in output]
        breaker = "* Radex version"
        breaker_lines = [i for i, line in enumerate(output) if breaker in line]
        breaker_lines.append(len(output))
        output_tables = []
        for i in range(1, len(breaker_lines)):
            lines_to_parse = output[breaker_lines[i - 1]:breaker_lines[i]]
            line_with_transitions = [
                outputline if bool(re.search(r"\d+\s*--\s*\d+", str(outputline))) else ""
                for outputline in lines_to_parse
            ]
            temp_tbl = astropy.io.ascii.read(line_with_transitions)
            output_tbl = Radex.add_labels_and_units(temp_tbl)
            output_tables.append(output_tbl)
        return output_tables

    @staticmethod
    def add_labels_and_units(temp_tbl):
        #  E_UP       FREQ        WAVEL     T_EX      TAU        T_R    \n', b'    POP        POP       FLUX        FLUX\n', b'                   (K)        (GHz)       (um)      (K)                  (K)    \n', b'     UP        LOW      (K*km/s) (erg/cm2/s)\n', b'1      -- 0          4.3     89.1884   3361.3393  -36.864 -2.943E-02  1.200E+00  2.224E-01  6.602E-02  6.386E+00  5.835E-08\n', b'2      -- 1         12.8    178.3748   1680.6883   48.307  8.720E-02  3.654E+00  3.105E-01  2.224E-01  1.945E+01  1.422E-06\n']
        output_tbl = astropy.table.table.Table()
        output_tbl['Transition'] = [f"{row['col1']}-{row['col3']}" for row in temp_tbl]
        output_tbl['E_UP'] = temp_tbl['col4'] * u.K
        output_tbl['FREQ'] = temp_tbl['col5'] * u.GHz
        output_tbl['WAVEL'] = temp_tbl['col6'] * u.um
        output_tbl['T_EX'] = temp_tbl['col7'] * u.K
        output_tbl['TAU'] = temp_tbl['col8']
        output_tbl['T_R'] = temp_tbl['col9'] * u.K
        output_tbl['POP_UP'] = temp_tbl['col10']
        output_tbl['POP_LOW'] = temp_tbl['col11']
        output_tbl['FLUX'] = temp_tbl['col12'] * u.K * u.km / u.s
        output_tbl['FLUX1'] = temp_tbl['col13'] * u.erg / u.cm ** 2 / u.s
        return output_tbl
