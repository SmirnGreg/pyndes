"""
This module contains parametrization of Bai & Goodman 2009 f.29 for Tx = 3 keV
"""
import numpy as np


def bai_goodman_09(radius, hydrogen_above, hydrogen_below=None):
    """
    Return ionization rate at given radius and amount of hydrogen above and below the point,
    according to Bai & Goodman 2009 f.29 for Tx = 3 keV
    Args:
        radius:
        hydrogen_above:
        hydrogen_below:

    Returns:
        ionization rate
    """
    zeta1 = 6e-12
    zeta2 = 1e-15
    n1 = 1.5e21
    n2 = 7e23
    alpha = 0.4
    beta = 0.65

    return (
                   zeta1 *
                   (
                           np.exp(-(hydrogen_above / n1) ** alpha) +
                           np.exp(-(hydrogen_below / n1) ** alpha)
                   ) + zeta2 *
                   (
                           np.exp(-(hydrogen_above / n2) ** beta) +
                           np.exp(-(hydrogen_below / n2) ** beta)
                   )
           ) / radius ** 2.2 * 1e-29


def demo():
    """Demo of bai_goodman_09"""
    from matplotlib import pyplot as plt

    hydrogen_max = 3.011063573172077E+024
    hydrogen_above = np.geomspace(1e15, hydrogen_max)

    plt.plot(
        hydrogen_above,
        1e30 * bai_goodman_09(
            1,
            hydrogen_above,
            2 * hydrogen_max - hydrogen_above
        )
    )
    plt.loglog()
    plt.show()


if __name__ == '__main__':
    demo()
