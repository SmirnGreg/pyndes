"""
This module contains parametrization of Bruderer et al. 2009 table 3
"""

import numpy as np

from pyndes.mathtools.interpolate import unsorted_interp2d

DENSITIES = np.logspace(20, 25, 6)
TEMPERATURES = np.array([3e6, 1e7, 3e7, 1e8, 3e8])
BRUDERER_TABLE = np.array(  # from Bruderer+2009 Table3
    [
        [4.8e-11, 2.4e-11, 1.0e-11, 3.4e-12, 1.2e-12],
        [4.2e-12, 4.5e-12, 2.5e-12, 9.8e-13, 3.7e-13],
        [6.9e-14, 3.7e-13, 4.0e-13, 2.2e-13, 1.0e-13],
        [6.9e-17, 9.6e-15, 3.9e-14, 4.5e-14, 3.3e-14],
        [6.8e-22, 4.5e-17, 2.0e-15, 9.4e-15, 1.4e-14],
        [1.8e-30, 8.6e-22, 1.7e-17, 1.4e-15, 6.4e-15]
    ]
)
"""Array representing Bruderer+09 table 3"""
INTERPOLATED_LOG_BRUDERER_TABLE = unsorted_interp2d(
    np.log10(DENSITIES), np.log10(TEMPERATURES), np.log10(BRUDERER_TABLE.T), kind='cubic'
)
"""Interpolated table 3 of Bruderer+09"""

def bruderer09(column_density, temperature=3e6):
    """Return x-ray ionization rate as of Bruderer+09 table 3"""
    log_column_density = np.log10(column_density)
    log_temperature = np.log10(temperature)
    out = 10 ** INTERPOLATED_LOG_BRUDERER_TABLE(
        log_column_density, log_temperature, assume_sorted=False
    )

    if len(out) == 1:
        return out[0]
    return out


# def bruderer_parametrized(column_density, temperature=3e6):
#     """As in
#     https://www.research-collection.ethz.ch/bitstream/handle/20.500.11850/152143/eth-1644-02.pdf?sequence=2&isAllowed=y
#     """
#     x = np.log10(column_density)
#     y = np.log10(temperature)
#     a = -17957.273149 + 6071.404798 * y - 685.922103 * y * y + 25.952759 * y * y * y
#     b = 1718.671224 - 583.303675 * y + 66.175787 * y * y - 2.516039 * y * y * y
#     c = -41.338087 + 14.079062 * y - 1.604290 * y * y + 0.061291 * y * y * y
#     return 3.56e-2 * 1e-30 * 4 * np.pi * (100 * 1.49597871e13) ** 2 * np.power(10, a + b * x + c * x * x)


def demo():
    """Demo of bruderer09"""
    from matplotlib import pyplot as plt
    temperature = np.geomspace(3e6, 3e8, 10)
    density = np.geomspace(1e22, 1e24, 100)
    for _temperature in temperature:
        plt.plot(
            density, bruderer09(density, _temperature),
            label=f'{_temperature:.2e}'
        )

    plt.loglog()
    plt.legend()
    plt.show()


if __name__ == '__main__':
    demo()
