"""
This module contains functions to integrate various functions
"""
import math

import autograd.numpy as np
from autograd import jacobian
import scipy.integrate


def integrate_on_line(function, r, z, r0, z0, rtol=1e-7, atol=1e-7, npoints=None):
    """
    Calculate 1st line integral of function on a straight line between r,z and r0,z0. If line passes z==0, add more points around.

    Args:
        function:
        r:
        z:
        r0:
        z0:
        rtol:
        atol:

    Returns:
        Integrated value
    """

    def radius(t):
        return np.array([r + (r0 - r) * t, z + (z0 - z) * t])

    if npoints is not None:
        t = np.linspace(0, 1, npoints)
        # radii_at_t = radius(t)
        function_at_t = function(*radius(t))
        integral = np.trapz(function_at_t, t) * ((r0 - r) ** 2 + (z0 - z) ** 2) ** 0.5
        return integral
    else:
        drdt = jacobian(radius)

        def integrand(t):
            function_here = function(*radius(t))
            # print(radius(t), function_here)
            return function_here * np.linalg.norm(drdt(t))

        if z != z0:
            midplane_t = z / (z - z0)
        elif z == 0:
            midplane_t = 0
        else:
            midplane_t = None
        # if radius(0)[1] * radius(1)[1] < 0:
        #     midplane_t = scipy.optimize.root_scalar(lambda t: radius(t)[1], bracket=[0, 1]).root
        # else:
        #     midplane_t = None
        # print(midplane_t)
        integral = scipy.integrate.quad(integrand, 0.0, 1.0, points=midplane_t, epsabs=atol, epsrel=rtol)
        return integral[0]
