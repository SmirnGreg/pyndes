"""
This package contains wrappers to mathematical functions from numpy, scipy and autograd
"""

from pyndes.mathtools import integrate
from pyndes.mathtools import interpolate