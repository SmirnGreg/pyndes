import numpy as np
import scipy.interpolate
from autograd import numpy as np


class NearestNDInterpolatorFillValue(scipy.interpolate.NearestNDInterpolator):
    """
    This class appears as NearestNDInterpolator, filling points outside the rectangle box of initial points with fill_value.
    Possibly returns f(x,-y) as f(x,y)
    """

    def __init__(self, x, y, *args, fill_value=None, abs_second_argument=False, **kwargs):
        """
        Interpolate data inside the given box, returning fill_value outside of the box.

        Args:
            x: Data point coordinates.
            y: Data values.
            *args: to be passed to scipy.interpolate.NearestNDInterpolator
            fill_value: value outside the given box
            abs_second_argument: if True, return f(x,-y) if y is negative
            **kwargs: to be passed to interpolate.NearestNDInterpolator
        """
        scipy.interpolate.NearestNDInterpolator.__init__(self, x, y, *args, **kwargs)
        self.fill_value = fill_value
        self.mins = [min(self.points[:, i]) for i in range(self.points.shape[1])]
        self.maxs = [max(self.points[:, i]) for i in range(self.points.shape[1])]
        self.abs_second_argument = abs_second_argument

    def __call__(self, *args):
        local_args = np.array(args)
        if self.abs_second_argument:
            local_args[1] = np.abs(local_args[1])
        if self.fill_value is not None:
            for i, arg in enumerate(local_args):
                if not self.mins[i] <= arg <= self.maxs[i]:
                    return self.fill_value
        return scipy.interpolate.NearestNDInterpolator.__call__(self, *args)


class LinearNDInterpolatorFillValue(scipy.interpolate.LinearNDInterpolator):
    """
    This class appears as LinearNDInterpolator, filling points outside the rectangle box of initial points with fill_value.
    Possibly returns f(x,-y) as f(x,y)
    """

    def __init__(self, x, y, *args, fill_value=None, abs_second_argument=False, **kwargs):
        """
        Interpolate data inside the given box, returning fill_value outside of the box.

        Args:
            x: Data point coordinates.
            y: Data values.
            *args: to be passed to scipy.interpolate.LinearNDInterpolator
            fill_value: value outside the given box
            abs_second_argument: if True, return f(x,-y) if y is negative
            **kwargs: to be passed to interpolate.LinearNDInterpolator
        """
        scipy.interpolate.LinearNDInterpolator.__init__(self, x, y, *args, **kwargs)
        self.fill_value = fill_value
        self.mins = [min(self.points[:, i]) for i in range(self.points.shape[1])]
        self.maxs = [max(self.points[:, i]) for i in range(self.points.shape[1])]
        self.abs_second_argument = abs_second_argument

    def __call__(self, *args):
        local_args = np.array(args)
        if self.abs_second_argument:
            local_args[1] = np.abs(local_args[1])
        if self.fill_value is not None:
            for i, arg in enumerate(local_args):
                if not self.mins[i] <= arg <= self.maxs[i]:
                    return self.fill_value
        return scipy.interpolate.LinearNDInterpolator.__call__(self, *args)

class unsorted_interp2d(scipy.interpolate.interp2d):
    """interp2d subclass that remembers original oxrder of data points"""

    def __call__(self, x, y, dx=0, dy=0, assume_sorted=None):
        unsorted_idxs = np.argsort(np.argsort(x))
        return scipy.interpolate.interp2d.__call__(self, x, y, dx=dx, dy=dy)[unsorted_idxs]
