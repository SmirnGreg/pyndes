import os
from copy import copy
from pprint import pprint
import sys
from warnings import warn

import numpy as np
from matplotlib import pyplot as plt
from astropy import units as u, coordinates
import astropy.table
from astroquery.alma import Alma, utils as almautils
from astroquery.splatalogue import Splatalogue
Splatalogue.QUERY_URL = 'https://splatalogue.online/c_export.php'
from astroquery.exceptions import InvalidQueryError


def get_frequencies_by_names(
        lines=(' CO ', ' 13CO '),
        min_freq=200 * u.GHz, max_freq=390 * u.GHz,
        energy_max=300,
        energy_type='eu_k',
        limit_transition_length=5,
        minimum_velocity_difference_kms=5.,
        **kwargs
):
    """
    This is a wrapper to astroquery.splatalogue that can query multiple species and returns less lines
    Args:
        lines: iterable with regular expressions for species name
        min_freq: minimal frequency to search
        max_freq: maximal frequency to search
        energy_max: maximal energy of the upper state
        energy_type: type of energy_max
        limit_transition_length: maximal length of string containing transition name (to avoid thin structure transitions)

    Returns:
        astropy.table.Table with splatalogue output

    """
    if isinstance(lines, str):
        lines_list = [lines]
    else:
        lines_list = list(lines)

    splatalogue_all_freqs_list_of_tables = []
    for line in lines_list:
        print(f"Quering line {line}...")
        splatalague_freqs = Splatalogue.query_lines(
            min_freq, max_freq, chemical_name=line,
            energy_max=energy_max,
            energy_type=energy_type,
            only_NRAO_recommended=True,
            # only_astronomically_observed=True,
            **kwargs,
        )  # ['Freq-GHz(rest frame,redshifted)']
        splatalague_freqs.remove_column('Lovas/AST Intensity')
        splatalague_freqs['Freq-GHz'] = 0.
        # splatalague_freqs.
        splatalague_freqs['Resolved QNs'] = np.vectorize(str)(splatalague_freqs['Resolved QNs'])
        rows_to_remove = []
        for i in range(len(splatalague_freqs)):
            try:
                if len(splatalague_freqs['Resolved QNs'][i].strip()) > limit_transition_length:
                    rows_to_remove.append(i)
                    warn(
                        f"Remove {line} {splatalague_freqs['Resolved QNs'][i]} because it is too long -- secondary transition?",
                        UserWarning
                    )
                    continue
            except AttributeError as e:
                print(e)
            if splatalague_freqs['Meas Freq-GHz(rest frame,redshifted)'][i]:
                # print(splatalague_freqs['Meas Freq-GHz(rest frame,redshifted)'][i])
                splatalague_freqs['Freq-GHz'][i] = splatalague_freqs['Meas Freq-GHz(rest frame,redshifted)'][i]
            else:
                splatalague_freqs['Freq-GHz'][i] = splatalague_freqs['Freq-GHz(rest frame,redshifted)'][i]
            try:
                if ((i - 1) not in rows_to_remove) and np.abs(
                        splatalague_freqs['Freq-GHz'][i] - splatalague_freqs['Freq-GHz'][i - 1]
                ) / splatalague_freqs['Freq-GHz'][i] * 3e5 < minimum_velocity_difference_kms:
                    rows_to_remove.append(i)
                    warn(
                        f"Remove {line} {splatalague_freqs['Resolved QNs'][i]} because it is close to a previous one -- secondary transition?",
                        UserWarning
                    )
                    continue
            except KeyError as e:
                print(e)

        print(f"Removing {len(rows_to_remove)} out of {len(splatalague_freqs)}.")
        splatalague_freqs.remove_rows(rows_to_remove)
        # splatalague_freqs['Freq-GHz'] = (
        #         splatalague_freqs['Meas Freq-GHz(rest frame,redshifted)']
        #         or splatalague_freqs['Freq-GHz(rest frame,redshifted)']
        # )
        splatalogue_all_freqs_list_of_tables.append(splatalague_freqs)
    splatalogue_all_freqs = astropy.table.vstack(splatalogue_all_freqs_list_of_tables)
    splatalogue_all_freqs.pprint(max_width=1000, max_lines=1000)
    print(f"In the above -- please check whether the found lines fit your request")
    return splatalogue_all_freqs


def query_alma_by_line_names(
        objects=None, lines=(' CO ', ' 13CO '),
        min_freq=50 * u.GHz, max_freq=500 * u.GHz,
        energy_max=300,
        energy_type='eu_k',
        limit_transition_length=5,
        all_sky=False,
        payload=None,
        cache_path=None,
        **kwargs
):
    if not (objects or all_sky):
        raise ValueError("No object list is provided. Use all_sky=True to query all the catalog.")

    if isinstance(objects, str):
        obj_list = [objects]
    elif objects is None:
        pass
    else:
        obj_list = list(objects)

    splatalogue_all_freqs = get_frequencies_by_names(
        lines=lines,
        min_freq=min_freq,
        max_freq=max_freq,
        energy_max=energy_max,
        energy_type=energy_type,
        limit_transition_length=limit_transition_length,
        **kwargs
    )

    if payload is None:
        payload = {}
    else:
        payload = dict(payload)

    payload['frequency'] = splatalogue_all_freqs['Freq-GHz']

    if 'science_keyword' not in payload.keys():
        payload['science_keyword'] = 'Disks around low-mass stars | Disks around high-mass stars | Astrochemistry'
    if 'spectral_resolution' not in payload.keys():
        payload['spectral_resolution'] = '<500'
    if 'spatial_resolution' not in payload.keys():
        payload['spatial_resolution'] = '<1'
    if 'line_sensitivity' not in payload.keys():
        payload['line_sensitivity'] = '<15'

    print("The selected payload is:")
    pprint(payload)
    myalma = Alma()
    if cache_path:
        myalma.cache_location = cache_path
        warn(f"Cache is stored into {cache_path}")
    obj_query_list = []
    if all_sky:
        print("Quering all sky...")
        obj_query = myalma.query(
            payload=payload,
            public=False,
        )
    else:
        for obj in obj_list:
            print(f"Quering {obj}...")
            single_obj_query = myalma.query_object(
                obj,
                payload=payload,
                public=False
            )
            obj_query_list.append(single_obj_query)
        obj_query = astropy.table.vstack(obj_query_list)

    print(' | '.join([str(freq) for freq in splatalogue_all_freqs['Freq-GHz']]))
    return obj_query


def query_prodige(
        file="alma.log",
        all_species=(
                ' HCO\+', ' DCO\+', ' H13CO\+ ', ' CO ', ' 13CO ', ' C18O ', ' 13C18O ', ' N2D\+ ',
                ' HC3N ', 'HCCCH', 'H2CO ', ' DCN ', ' HCN ', ' HNC ', ' DNC ', ' SO ',
                ' SO2 ', ' CS ', ' HDCO ', ' D2CO ', ' 13CN ', ' CCH ', ' N2H\+ ', ' OCS ',
                ' H2CS ', ' H13CN ', ' H13CO\+ ', ' HN13C ',
        ),
        object_list=(
                "CI Tau", "CY Tau", "DG Tau", "DL Tau", "DM Tau", "DN Tau", "IQ Tau", "UZ Tau E", "GO Tau"
        ),
        payload=(
                ('spectral_resolution', '<500'),  # kHz ~ 1m/s
                ('spatial_resolution', '<0.7'),  # arcsec
                ('line_sensitivity', '<100'),  # mJy/beam ~ ?? K
        ),
        filename="out.html",
        short=False,
        cache_path=None,
):
    print(cache_path)
    warn(cache_path)
    from tqdm import tqdm
    all_lines = get_frequencies_by_names(
        all_species,
        min_freq=200 * u.GHz, max_freq=400 * u.GHz,
        limit_transition_length=100,
        energy_max=150,
    )
    payload = dict(payload)
    with open(file, 'w') as fff:
        original_stdout = sys.stdout
        sys.stdout = fff
        all_tables = {}
        for spec in tqdm(all_species, desc="Querying ALMA archive"):
            try:
                tbl = query_alma_by_line_names(
                    lines=spec,
                    all_sky=False,
                    objects=object_list,
                    payload=payload,
                    limit_transition_length=100,
                    cache_path=cache_path,
                )
                tbl.pprint(max_width=5000, max_lines=1000)
                print("\n\n\n")
                all_tables[spec] = tbl
            except InvalidQueryError as e:
                print(f"{e}")
        general_table = astropy.table.vstack(list(all_tables.values()))
        general_table.pprint(max_width=5000, max_lines=1000)
        # general_table.show_in_browser(jsviewer=True)

        reduced_table = general_table['Source name', 'RA', 'Dec', 'Release date']

        for table_name in all_tables.keys():
            reduced_table[table_name] = 10000 * ' '
            reduced_table[table_name] = ''
        reduced_table['Combined'] = 0
        output_table = reduced_table[[]]
        total_columns = len(output_table.columns)

        reduced_table.sort('RA')

        # plt.plot(reduced_table['RA'],reduced_table['Dec'], 'ro')
        # plt.show()

        previous_coord = coordinates.SkyCoord(0 * u.deg, 90 * u.deg)
        all_projects = set()
        for entry in tqdm(reduced_table, desc="Parsing tables"):
            this_ra = entry['RA'] * u.deg
            this_dec = entry['Dec'] * u.deg
            this_coord = coordinates.SkyCoord(this_ra, this_dec)

            if this_coord.separation(previous_coord) < 2 * u.arcsec:
                continue
            # if entry['Release date'] != '':
            #     continue

            row = [entry['Source name'], this_ra, this_dec]
            row.extend((total_columns - len(row)) * [0])
            output_table.add_row()
            output_table['Source name'][-1] = entry['Source name']
            output_table['RA'][-1] = entry['RA']
            output_table['Dec'][-1] = entry['Dec']

            for table_name in (all_tables.keys()):
                found_in_table = False
                projects = ''
                projects_list = []
                for other_entry in all_tables[table_name]:
                    other_ra = other_entry['RA'] * u.deg
                    other_dec = other_entry['Dec'] * u.deg
                    other_coord = coordinates.SkyCoord(
                        other_ra, other_dec
                    )
                    if this_coord.separation(other_coord) < 2 * u.arcsec:
                        found_in_table = True
                        # found_transition = all_lines['Resolved QNs'][all_lines['Freq-Ghz']]
                        found_transitions = []
                        for line in all_lines:
                            for spw in almautils.parse_frequency_support(other_entry['Frequency support']):
                                if spw[0] < line['Freq-GHz'] * u.GHz < spw[1]:
                                    found_transitions.append(line['Species'] + ':' + str(line['Resolved QNs']))
                        if short:
                            project_string = other_entry['Project code']
                        else:
                            project_string = other_entry['Project code'] + '_[' + '] _ ['.join(
                                found_transitions) + ']' + '    \n\n\n'
                        projects_list.append(project_string)
                        all_projects.add(other_entry['Project code'])
                if found_in_table:
                    output_table[table_name][-1] = ', '.join(projects_list)
                    output_table['Combined'][-1] += 1
                else:
                    output_table[table_name][-1] = '_None'
            # output_table['Combined']
            previous_coord = this_coord

        output_table.pprint(max_width=5000, max_lines=1000)
        try:
            os.remove(filename)
        except FileNotFoundError:
            pass
        finally:
            output_table.write(
                filename, format='jsviewer',
                max_lines=-1,
                table_class='astropy-default',
            )
        print("\n\n\n")
        sys.stdout = original_stdout
        print(f"Projects found: {'|'.join(all_projects)}")


def query_almaproposal2019():
    file = "alma.log"
    all_lines = get_frequencies_by_names(
        [' CS ', ' HCO\+', ' H13CO\+', ' DCO\+'],
        min_freq=50 * u.GHz, max_freq=500 * u.GHz,
    )
    with open(file, 'w') as fff:
        original_stdout = sys.stdout
        sys.stdout = fff
        tbl_cs = query_alma_by_line_names(
            lines=[' CS '], all_sky=True,
            payload={
                'spectral_resolution': '<100',
                'spatial_resolution': '<0.3',
            }
        )
        tbl_cs.pprint(max_width=5000, max_lines=1000)

        print("\n\n\n")

        tbl_cs_not_strict = query_alma_by_line_names(
            lines=[' CS '], all_sky=True,
            payload={
                'spectral_resolution': '<200',
                'spatial_resolution': '<0.5',
            }
        )
        tbl_cs_not_strict.pprint(max_width=5000, max_lines=1000)

        print("\n\n\n")

        tbl_cs_not_strict_at_all = query_alma_by_line_names(
            lines=[' CS '], all_sky=True,
            payload={
                'spectral_resolution': '<500',
                'spatial_resolution': '<0.7',
            }
        )
        tbl_cs_not_strict_at_all.pprint(max_width=5000, max_lines=1000)

        print("\n\n\n")

        tbl_hcop = query_alma_by_line_names(
            lines=[' HCO\+'], all_sky=True,
            payload={
                'spectral_resolution': '<500',
                'spatial_resolution': '<0.7',
                'line_sensitivity': '<100'
            }
        )
        tbl_hcop.pprint(max_width=5000, max_lines=1000)

        print("\n\n\n")

        tbl_h13cop = query_alma_by_line_names(
            lines=[' H13CO\+'], all_sky=True,
            payload={
                'spectral_resolution': '<500',
                'spatial_resolution': '<0.7',
            }
        )
        tbl_h13cop.pprint(max_width=5000, max_lines=1000)

        print("\n\n\n")

        tbl_dcop = query_alma_by_line_names(
            lines=[' DCO\+'], all_sky=True,
            payload={
                'spectral_resolution': '<500',
                'spatial_resolution': '<0.7',
            }
        )
        tbl_dcop.pprint(max_width=5000, max_lines=1000)
        print("\n\n\n")

        all_tables = {
            'CS 100 kHz 0.3"': tbl_cs,
            'CS 200 kHz 0.5"': tbl_cs_not_strict,
            'CS 500 kHz 0.7"': tbl_cs_not_strict_at_all,
            'HCO+': tbl_hcop,
            'DCO+': tbl_dcop,
            'H13CO+': tbl_h13cop
        }

        general_table = astropy.table.vstack(list(all_tables.values()))
        general_table.pprint(max_width=5000, max_lines=1000)
        # general_table.show_in_browser(jsviewer=True)

        reduced_table = general_table['Source name', 'RA', 'Dec', 'Release date']

        for table_name in all_tables.keys():
            reduced_table[table_name] = 10000 * ' '
            reduced_table[table_name] = ''
        reduced_table['Combined'] = 0
        output_table = reduced_table[[]]
        total_columns = len(output_table.columns)

        reduced_table.sort('RA')

        # plt.plot(reduced_table['RA'],reduced_table['Dec'], 'ro')
        # plt.show()

        previous_coord = coordinates.SkyCoord(0 * u.deg, 90 * u.deg)
        for entry in reduced_table:
            this_ra = entry['RA'] * u.deg
            this_dec = entry['Dec'] * u.deg
            this_coord = coordinates.SkyCoord(this_ra, this_dec)

            if this_coord.separation(previous_coord) < 2 * u.arcsec:
                continue
            # if entry['Release date'] != '':
            #     continue

            row = [entry['Source name'], this_ra, this_dec]
            row.extend((total_columns - len(row)) * [0])
            output_table.add_row()
            output_table['Source name'][-1] = entry['Source name']
            output_table['RA'][-1] = entry['RA']
            output_table['Dec'][-1] = entry['Dec']

            for table_name in all_tables.keys():
                found_in_table = False
                projects = ''
                projects_list = []
                for other_entry in all_tables[table_name]:
                    other_ra = other_entry['RA'] * u.deg
                    other_dec = other_entry['Dec'] * u.deg
                    other_coord = coordinates.SkyCoord(
                        other_ra, other_dec
                    )
                    if this_coord.separation(other_coord) < 2 * u.arcsec:
                        found_in_table = True
                        # found_transition = all_lines['Resolved QNs'][all_lines['Freq-Ghz']]
                        found_transitions = []
                        for line in all_lines:
                            for spw in almautils.parse_frequency_support(other_entry['Frequency support']):
                                if spw[0] < line['Freq-GHz'] * u.GHz < spw[1]:
                                    found_transitions.append(line['Species'] + line['Resolved QNs'])
                        projects_list.append(other_entry['Project code'] + '_' + '_'.join(found_transitions))
                if found_in_table:
                    output_table[table_name][-1] = ', '.join(projects_list)
                    output_table['Combined'][-1] += 1
                else:
                    output_table[table_name][-1] = '_None'
            # output_table['Combined']
            previous_coord = this_coord

        output_table.pprint(max_width=5000, max_lines=1000)
        # output_table.show_in_browser(jsviewer=True)
        filename = "query/out.html"
        try:
            os.remove(filename)
        except FileNotFoundError:
            pass
        finally:
            output_table.write(
                filename, format='jsviewer',
                max_lines=-1,
                table_class='astropy-default',
            )
        print("\n\n\n")

        sys.stdout = original_stdout


# def test_alma_splatalogue():
#     query_alma_by_line_names('TW Hya')


if __name__ == '__main__':
    query_prodige()
