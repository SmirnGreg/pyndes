#!/usr/bin/env python3
import glob

from astropy.io.ascii import read
from os import listdir
from os.path import join
import os
from warnings import warn
import re
from tqdm import tqdm
import psutil


def read_one_file(file_location, only_last=True, fix_cm2au=False):
    """
    This function is terribly non-optimized (multiple joins)
    :param file_location:
    :param only_last:
    :param fix_cm2au:
    :return:
    """
    lines_to_skip_after_header = 1  # 1st entry
    lines_to_skip_after_header += 2  # and ---

    file = open(file_location, 'r')
    initial_values = {}
    do_initial_values = False
    for line in file:
        if 'INITIAL VALUES' in line:
            do_initial_values = True
            continue
        if do_initial_values:
            line_splitted = line.split('=')
            if len(line_splitted) > 1:
                value = line_splitted[1].split()[0]
                if value[-1] == 'K':
                    value = value[:-1]  # Remove K character on the end of the value to convert later
                initial_values[line_splitted[0].strip()] = float(value)
        if 'CALCULATED ABUNDANCES' in line:
            break
        if 'failed after' in line:
            file.close()
            return None, None
    if fix_cm2au:
        initial_values['Radius'] /= 1.49597871e13
        initial_values['Height'] /= 1.49597871e13
    table = file.readlines()
    file.close()
    table_len = len(table)
    header_line = ''
    data_lines = ['', ]
    i = 0
    while i < table_len:
        if table[i].strip() == '':
            # print('end: {}/{}'.format(i, table_len))
            i += 1
            continue
        # If table line is > 3 replace time by timei
        header_entry = (table[i].rstrip()).replace('time', 'time{}'.format(i if i > 3 else ''))
        header_line += '|'.join(header_entry.split()) + '|'
        # print(i, table[i])
        j = lines_to_skip_after_header  # Go 2 lines down
        while not (' '.join(table[i + j].split()) in ' '.join(table[i].split())):
            # print(i + j, table[i + j])            '
            if len(data_lines) < j - lines_to_skip_after_header + 1:
                data_lines.append('')
            data_lines[j - lines_to_skip_after_header] += '|'.join(table[i + j].split()) + '|'
            j += 1

        i += j + 1
    keys = sorted(list(initial_values.keys()))
    header_line = '|'.join(keys) + '|' + header_line[:-1]
    for i, data_line in enumerate(data_lines):
        data_lines[i] = '|'.join([str(initial_values[key]) for key in keys]) + '|' + data_line[:-1]
    if only_last:
        data_lines = [data_lines[-1]]
    return header_line, data_lines


def between_underscore_and_dot(string):
    """
    Returns integer number from string, located between first right underscore (prefix) and dot (extention)
    The purpose of the function is to extract file number from its filename

    Args:
        string: string to be processed

    Returns:
        integer number from string

    """
    underscore_loc = string.rfind('_')
    dot_loc = string.rfind('.')
    return int(string[underscore_loc + 1:dot_loc])


def one_dir_get(
        directory, file_out='all_abundances.dat', header=True, maxfiles=None, fix_cm2au=False, write=True,
        **kwargs
):
    """
    Takes all files with extention ".out" from the directory, composes one astropy table, and writes ot to file_out
    Args:
        directory: directory to be processed
        file_out: name of the output file, or file-like structure
        header: write table header if True
        maxfiles: limit of files to be read

    Returns:
        None

    """
    files = []
    for file in listdir(directory):
        if file.endswith(".out") and "slurm" not in file:
            files.append(join(directory, file))
            # print(between_underscore_and_dot(file))

    files = sorted(files, key=between_underscore_and_dot)
    if not files:
        raise FileNotFoundError
    if maxfiles: files = files[:maxfiles]
    headers = []
    data = []

    for file in tqdm(files):
        header_line, data_lines = read_one_file(file, fix_cm2au=fix_cm2au)
        if header_line is None or data_lines is None:
            continue
        headers.append(header_line)
        data.extend(data_lines)
        if psutil.virtual_memory().percent > 90.:
            raise MemoryError("Not enough memory!")

    if len(set(headers)) > 1:
        warn("Tables in Output/out have different structure!")

    table_in = [headers[0]] + data
    # extracted_table = header_line + '\n' + '\n'.join(data_lines)
    table = read(table_in, delimiter='|')
    for colname in table.colnames:
        if re.match(r'time\d+', colname):
            table.remove_column(colname)
        else:
            table[colname].format = "16.4e"
    # return table
    if header:
        fmt = 'ascii.fixed_width'
    else:
        fmt = 'ascii.fixed_width_no_header'
    if write: table.write(file_out, format=fmt, overwrite=True, **kwargs)
    return table


def compose_all_subdirectories(
        main_directory, template="Chem???", file_out='chemical_structure.dat',
        fix_cm2au=False
):
    """
    Takes all subfolders that match template, runs one_dir_get for each of them, and saves to one file
    Args:
        main_directory: the directory to be processed
        template: name template of subdirectories
        file_out: string with filename for output

    Returns:
        None
    """
    with open(file_out, 'w') as ftable:
        for i, subdir in enumerate(sorted(glob.glob(os.path.join(main_directory, template)))):
            print(subdir)
            one_dir_get(os.path.join(subdir, 'out'), file_out=ftable, header=not i, fix_cm2au=fix_cm2au)
