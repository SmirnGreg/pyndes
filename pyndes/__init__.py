"""
This repository contains python and other code that may be used in a large number of projects of the author, Grigorii Smirnov-Pinchukov.

You better don't use it without asking the author (smirnov@mpia.de), mostly because this code has even less warranty than you could expect.
"""

from pyndes import ionization
from pyndes import mathtools
from pyndes import andes
