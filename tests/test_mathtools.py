import math
import pytest
import numpy as np

from scipy.interpolate import LinearNDInterpolator

from pyndes.mathtools.integrate import integrate_on_line
from pyndes.mathtools.interpolate import NearestNDInterpolatorFillValue, LinearNDInterpolatorFillValue


@pytest.mark.parametrize(
    "value, x, y, x0, y0, desired",
    [
        (1, 0, 0, 1, 0, 1),
        (1, 1, 0, 0, 0, 1),
        (2, 1, 0, 0, 0, 2),
        (math.sqrt(2), 0, 0, 1, 1, 2),
    ]
)
def test_integrate_on_line_constant(value, x, y, x0, y0, desired):
    assert integrate_on_line(lambda x, y: value, x, y, x0, y0) == pytest.approx(desired)


@pytest.mark.parametrize(
    "power, desired",
    [
        (0, 2 * math.sqrt(2)),
        (1, math.sqrt(2)),
        (2, 2. / 3 * math.sqrt(2)),
    ]
)
def test_integrate_power(power, desired):
    assert integrate_on_line(lambda x, y: x ** power + y ** power, 0, 0, 1, 1) == pytest.approx(desired)


X1 = np.array([1, 1, 3, 3, 6, 6])
Y1 = np.array([0, 2, 0, 3, 0, 5])
FOO1 = X1 ** 2 - Y1 ** 2


def test_integrate_analytical_for_interpolated():
    assert integrate_on_line(lambda x, y: x ** 2 - y ** 3, 0, 0, 1, 2) == pytest.approx(-5. * math.sqrt(5.) / 3.)


X = np.linspace(-10, 10, 100)
Y = np.linspace(-10, 10, 100)
Xmesh, Ymesh = np.meshgrid(X, Y)
Zmesh = (lambda x, y: x ** 2 - y ** 3)(Xmesh, Ymesh)


def test_integrate_nearest_interpolated():
    interpolated_z = NearestNDInterpolatorFillValue((Xmesh.ravel(), Ymesh.ravel()), Zmesh.ravel())
    assert integrate_on_line(interpolated_z, 0, 0, 1, 2) == pytest.approx(-5. * math.sqrt(5.) / 3., rel=1e-2)

def test_integrate_linear_interploated_scipy():
    interpolated_z = LinearNDInterpolator((Xmesh.ravel(), Ymesh.ravel()), Zmesh.ravel())
    assert integrate_on_line(interpolated_z, 0., 0., 1., 2.) == pytest.approx(-5. * math.sqrt(5.) / 3., rel=1e-2)

def test_fast_integrate_linear_interploated_scipy():
    interpolated_z = LinearNDInterpolator((Xmesh.ravel(), Ymesh.ravel()), Zmesh.ravel())
    assert integrate_on_line(interpolated_z, 0., 0., 1., 2., npoints=100) == pytest.approx(-5. * math.sqrt(5.) / 3., rel=1e-2)


# def test_integrate_linear_interpolated():
#     interpolated_z = LinearNDInterpolatorFillValue((Xmesh.ravel(), Ymesh.ravel()), Zmesh.ravel())
#     assert integrate_on_line(interpolated_z, 0., 0., 1., 2.) == pytest.approx(-5. * math.sqrt(5.) / 3., rel=1e-2)


@pytest.mark.parametrize(
    "x, y, desired",
    [
        (1, 0, 1),
        (1, 2, -3),
        (1.1, 2.2, -3),
    ]
)
# @pytest.mark.parametrize(
#     "do_numba",
#     [
#         True, False
#     ]
# )
def test_NearestNDInterpolatorFillValue(x, y, desired):
    fun = NearestNDInterpolatorFillValue((X1, Y1), FOO1)
    assert fun(x, y) == desired

# def test_numba_is_faster():
#     fun = NearestNDInterpolatorFillValue((X1, Y1), FOO1)
#     fun_numba = NearestNDInterpolatorFillValue((X1, Y1), FOO1, do_numba=True)
#
#     assert fun(1, 0) == 1
#     assert fun_numba(1, 0) == 1
#
#     n = 100000
#
#     time0 = time.time()
#     for i in range(n):
#         out = fun(1, 0)
#     dt1 = time.time() - time0
#     for i in range(n):
#         out_num = fun_numba(1, 0)
#     dt2 = time.time() - time0 - dt1
#     print(f"Non-numba took {dt1}, while numba tool {dt2}")

