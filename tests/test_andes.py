from pyndes.andes.core import generate_abundance
import pytest
# import hypothesis
# import hypothesis.strategies


@pytest.mark.parametrize(
    "input, expected",
    (
            ("hello_123.txt", 123),
            ("_123.ttt", 123),
            ("_124.cov_123.txttxt", 123),
            ("hello_world_123.txt", 123),
    )
)
def test_between_underscore_and_dot(input, expected):
    assert generate_abundance.between_underscore_and_dot(input) == expected


# @hypothesis.given(
#     s1=hypothesis.strategies.text(
#         alphabet=hypothesis.strategies.characters(blacklist_categories=("Lo", "M", "C"))
#     ),
#     s2=hypothesis.strategies.integers(),
#     s3=hypothesis.strategies.text(
#         alphabet=hypothesis.strategies.characters(blacklist_categories=("Lo", "M", "C"), blacklist_characters="."),
#     )
# )
# def test_between_underscore_and_dot_hypothesis(s1, s2, s3):
#     long_str = f"{s1}_{s2}.{s3}"
#     hypothesis.event(f"{__name__}.test_between_underscore_and_dot_hypothesis: {long_str}")
#     assert generate_abundance.between_underscore_and_dot(long_str) == int(s2)
