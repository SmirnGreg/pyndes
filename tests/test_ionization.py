import numpy as np
from astropy.table import Column
from pyndes.ionization.bruderer import bruderer09
from pyndes.ionization.bai import bai_goodman_09
from pyndes.ionization.rab_crp import rab_17
import pytest


@pytest.mark.parametrize(
    "density, temperature, expected",
    [
        (1e20, 3e6, 4.8e-11),
        (1e22, 3e6, 6.9e-14),
        (1e23, 3e7, 3.9e-14),
        (2e23, 2e7, 1.2178875179615492e-14),
        (1e22, 5e6, 1.1148990808141437e-13),
        (1e24, 2e7, 3.000000000000001e-16),
        (Column([1e20, 1e22, 1e24]), 3e6, np.array([4.8e-11, 6.9e-14, 6.8e-22])),
        (Column([1e20, 1e22, 1e24]), 3e6, np.array([4.8e-11, 6.8e-22, 6.9e-14])),
    ]
)
def test_bruderer09(density, temperature, expected):
    assert bruderer09(density, temperature) == pytest.approx(expected, rel=1e-1) \
 \
        # @pytest.mark.parametrize(


#     "density, temperature, expected",
#     [
#         (1e20, 3e6, 4.8e-11),
#         (1e22, 3e6, 6.9e-14),
#         (1e23, 3e7, 3.9e-14),
#     ]
# )
# def test_bruderer_parametrized(density, temperature, expected):
#     assert bruderer_parametrized(density, temperature) == pytest.approx(expected, rel=1e-3)

@pytest.mark.parametrize(
    "radius, hydrogen_above, hydrogen_below, expected",
    [
        (1, 1e22, 1e22, 1.4e-41),
        (10, 1e19, 1e20, 6.0e-43),
        (100, 1e17, 1e18, 4.6e-45),
    ]
)
def test_bai(radius, hydrogen_above, hydrogen_below, expected):
    assert bai_goodman_09(radius, hydrogen_above, hydrogen_below) == pytest.approx(expected)


@pytest.mark.parametrize(
    "column_density, expected, mode",
    [
        (np.logspace(10, 20, 10),
         np.array([1.18933956e-09, 1.18933956e-09, 1.18933956e-09, 1.18933956e-09, 1.18933956e-09, 5.45025694e-10,
                   1.14456167e-10, 2.40359571e-11, 5.04758498e-12, 1.05999865e-12]), "Sun"),
        (1e10, 1.18933956e-09, "Sun"),
        (1e16, 2.9194824255384185e-10, "Sun"),
        (1e19, 4.318230889839605e-12, "Sun"),
        (1e20, 1.05999865e-12, "Sun"),
        (np.logspace(10, 20, 10),
         np.array([1.18933956e-04, 1.18933956e-04, 1.18933956e-04, 1.18933956e-04, 1.18933956e-04, 5.45025694e-05,
                   1.14456167e-05, 2.40359571e-06, 5.04758498e-07, 1.05999865e-07]),
         "T Tauri"),
        (1e10, 0.00011893395615600808, "T Tauri"),
        (1e16, 2.919482425538419e-05, "T Tauri"),
        (1e19, 4.3182308898396047e-07, "T Tauri"),
        (1e20, 1.0599986527595061e-07, "T Tauri"),

    ]
)
def test_rab_17(column_density, expected, mode):
    assert rab_17(column_density, mode) == pytest.approx(expected)


@pytest.mark.parametrize(
    "mode",
    [
        None,
        "Default",
        1,
        0,
        "TTauri"
    ]
)
def test_rab_17_badmode(mode):
    pytest.raises(ValueError, rab_17, 1e10, mode)
