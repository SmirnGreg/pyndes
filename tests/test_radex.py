import pytest
from astropy import units as u
import numpy.testing

from pyndes.radexpy.radex import get_output_table, PATH_RADEX, RADEX_IN_PATH
from pyndes.radexpy.engine import Radex


@pytest.mark.skipif(not RADEX_IN_PATH, reason='Radex is not found in PATH')
@pytest.mark.parametrize(
    "inp,expected",
    (
            (
                    {
                        'molecular_data_file': 'hco+.dat',
                        'kinetic_temperature': 40 * u.K,
                        'molecular_column_density': 1e13 * u.cm ** -2,
                        'executable': PATH_RADEX,
                        'temporary_file_kwargs': {'dir': '.'},
                    },
                    [2.253e+00, 1.015e+00, 4.052e-01, 1.380e-01, 4.387e-02, 1.436e-02,
                     4.516e-03, 1.247e-03, 2.693e-04, 5.149e-05, 1.020e-05]
            ),
            (
                    {
                        'molecular_data_file': 'hco+.dat',
                        'kinetic_temperature': 40 * u.K,
                        'molecular_column_density': 1e13 * u.cm ** -2 * 1e-2,
                        'executable': PATH_RADEX,
                        'temporary_file_kwargs': {'dir': '.'},
                    },
                    [4.294e-02, 1.539e-02, 4.504e-03, 1.350e-03, 4.223e-04, 1.392e-04,
                     4.440e-05, 1.229e-05, 2.601e-06, 4.954e-07, 9.788e-08]
            ),
            (
                    {
                        'molecular_data_file': 'hco+@xpol.dat',
                        'kinetic_temperature': 40 * u.K,
                        'molecular_column_density': 1e13 * u.cm ** -2,
                        'executable': PATH_RADEX,
                        'temporary_file_kwargs': {'dir': '.'},
                    },
                    [2.258e+00, 9.874e-01, 3.919e-01, 1.351e-01, 4.239e-02, 1.384e-02,
                     4.332e-03, 1.187e-03, 2.596e-04, 5.067e-05, 1.007e-05]
            ),
            (
                    {
                        'molecular_data_file': 'hco+@xpol.dat',
                        'kinetic_temperature': 40 * u.K,
                        'molecular_column_density': 1e13 * u.cm ** -2 * 1e-2,
                        'executable': PATH_RADEX,
                        'temporary_file_kwargs': {'dir': '.'},
                    },
                    [4.298e-02, 1.502e-02, 4.377e-03, 1.329e-03, 4.109e-04, 1.352e-04,
                     4.295e-05, 1.180e-05, 2.524e-06, 4.895e-07, 9.685e-08]
            )
    )
)
def test_radex(inp, expected):
    out = get_output_table(**inp)
    numpy.testing.assert_array_almost_equal(out['FLUX'], expected)


@pytest.mark.skipif(not RADEX_IN_PATH, reason='Radex is not found in PATH')
@pytest.mark.parametrize(
    "inp,expected",
    (
            (
                    {
                        'molecular_data_file': 'hco+@xpol.dat',
                        'kinetic_temperature': [40 * u.K, 40 * u.K],
                        'molecular_column_density': [1e13 * u.cm ** -2, 1e13 * u.cm ** -2 * 1e-2],
                        'executable': PATH_RADEX,
                        'temporary_file_kwargs': {'dir': '.'},
                    },
                    [2.258e+00, 9.874e-01, 3.919e-01, 1.351e-01, 4.239e-02, 1.384e-02,
                     4.332e-03, 1.187e-03, 2.596e-04, 5.067e-05, 1.007e-05, 4.298e-02, 1.502e-02, 4.377e-03, 1.329e-03,
                     4.109e-04, 1.352e-04, 4.295e-05, 1.180e-05, 2.524e-06, 4.895e-07, 9.685e-08]
            ),
    )
)
def test_radex_multiple(inp, expected):
    rdx = Radex(executable=inp.pop('executable'))
    out = rdx.call_multiple(**inp)
    numpy.testing.assert_array_almost_equal(out['FLUX'], expected)


EXAMPLE_TWO_OUTPUT = [
    b'* Radex version        : 30nov2011\n', b'* Geometry             : Uniform sphere\n',
    b'* Molecular data file  : /Radex/data/hco+@xpol.dat                                                       \n',
    b'* T(kin)            [K]:   40.000\n', b'* Density of H2  [cm-3]:  1.000E+04\n',
    b'* T(background)     [K]:    2.730\n', b'* Column density [cm-2]:  1.000E+13\n',
    b'* Line width     [km/s]:    1.000\n', b'Calculation finished in   28 iterations\n',
    b'      LINE         E_UP       FREQ        WAVEL     T_EX      TAU        T_R       POP        POP       FLUX        FLUX\n',
    b'                   (K)        (GHz)       (um)      (K)                  (K)        UP        LOW      (K*km/s) (erg/cm2/s)\n',
    b'1      -- 0          4.3     89.1885   3361.3345    5.138  3.909E+00  2.121E+00  4.982E-01  3.820E-01  2.258E+00  2.063E-08\n',
    b'2      -- 1         12.8    178.3751   1680.6859    4.255  5.208E+00  9.275E-01  1.110E-01  4.982E-01  9.874E-01  7.217E-08\n',
    b'3      -- 2         25.7    267.5576   1120.4781    4.248  1.147E+00  3.682E-01  7.564E-03  1.110E-01  3.919E-01  9.667E-08\n',
    b'4      -- 3         42.8    356.7343    840.3803    7.429  7.044E-02  1.269E-01  9.707E-04  7.564E-03  1.351E-01  7.898E-08\n',
    b'5      -- 4         64.2    445.9030    672.3266   12.953  7.893E-03  3.982E-02  2.274E-04  9.707E-04  4.239E-02  4.840E-08\n',
    b'6      -- 5         89.9    535.0618    560.2950   17.252  1.739E-03  1.300E-02  6.065E-05  2.274E-04  1.384E-02  2.730E-08\n',
    b'7      -- 6        119.8    624.2087    480.2760   20.373  4.554E-04  4.069E-03  1.608E-05  6.065E-05  4.332E-03  1.357E-08\n',
    b'8      -- 7        154.1    713.3421    420.2646   21.920  1.227E-04  1.115E-03  3.823E-06  1.608E-05  1.187E-03  5.550E-09\n',
    b'9      -- 8        192.6    802.4583    373.5926   21.935  3.032E-05  2.439E-04  7.383E-07  3.823E-06  2.596E-04  1.728E-09\n',
    b'10     -- 9        235.4    891.5579    336.2568   23.199  5.923E-06  4.760E-05  1.290E-07  7.383E-07  5.067E-05  4.625E-10\n',
    b'11     -- 10       282.4    980.6374    305.7118   26.057  1.023E-06  9.460E-06  2.322E-08  1.290E-07  1.007E-05  1.223E-10\n',
    b'* Radex version        : 30nov2011\n', b'* Geometry             : Uniform sphere\n',
    b'* Molecular data file  : /Radex/data/hco+@xpol.dat                                                       \n',
    b'* T(kin)            [K]:   40.000\n', b'* Density of H2  [cm-3]:  1.000E+04\n',
    b'* T(background)     [K]:    2.730\n', b'* Column density [cm-2]:  1.000E+11\n',
    b'* Line width     [km/s]:    1.000\n', b'Calculation finished in   13 iterations\n',
    b'      LINE         E_UP       FREQ        WAVEL     T_EX      TAU        T_R       POP        POP       FLUX        FLUX\n',
    b'                   (K)        (GHz)       (um)      (K)                  (K)        UP        LOW      (K*km/s) (erg/cm2/s)\n',
    b'1      -- 0          4.3     89.1885   3361.3345    3.493  6.389E-02  4.037E-02  4.402E-01  4.996E-01  4.298E-02  3.926E-10\n',
    b'2      -- 1         12.8    178.3751   1680.6859    3.286  4.920E-02  1.411E-02  5.420E-02  4.402E-01  1.502E-02  1.098E-09\n',
    b'3      -- 2         25.7    267.5576   1120.4781    4.647  5.517E-03  4.112E-03  4.789E-03  5.420E-02  4.377E-03  1.080E-09\n',
    b'4      -- 3         42.8    356.7343    840.3803    8.987  4.217E-04  1.248E-03  9.163E-04  4.789E-03  1.329E-03  7.769E-10\n',
    b'5      -- 4         64.2    445.9030    672.3266   13.133  7.410E-05  3.860E-04  2.195E-04  9.163E-04  4.109E-04  4.692E-10\n',
    b'6      -- 5         89.9    535.0618    560.2950   17.379  1.674E-05  1.270E-04  5.921E-05  2.195E-04  1.352E-04  2.667E-10\n',
    b'7      -- 6        119.8    624.2087    480.2760   20.589  4.424E-06  4.035E-05  1.594E-05  5.921E-05  4.295E-05  1.345E-10\n',
    b'8      -- 7        154.1    713.3421    420.2646   21.958  1.216E-06  1.109E-05  3.800E-06  1.594E-05  1.180E-05  5.517E-11\n',
    b'9      -- 8        192.6    802.4583    373.5926   21.663  3.027E-07  2.371E-06  7.179E-07  3.800E-06  2.524E-06  1.680E-11\n',
    b'10     -- 9        235.4    891.5579    336.2568   23.118  5.766E-08  4.598E-07  1.247E-07  7.179E-07  4.895E-07  4.467E-12\n',
    b'11     -- 10       282.4    980.6374    305.7118   25.993  9.887E-09  9.099E-08  2.233E-08  1.247E-07  9.685E-08  1.176E-12\n'
]


@pytest.mark.parametrize(
    "inp, expected",
    (
            (EXAMPLE_TWO_OUTPUT,
             (
                     (
                             2.258E+00, 9.874E-01, 3.919E-01, 1.351E-01, 4.239E-02, 1.384E-02, 4.332E-03, 1.187E-03,
                             2.596E-04, 5.067E-05, 1.007E-05
                     ),
                     (
                             4.298E-02, 1.502E-02, 4.377E-03, 1.329E-03, 4.109E-04, 1.352E-04, 4.295E-05, 1.180E-05,
                             2.524E-06, 4.895E-07, 9.685E-08,
                     ),
             ),
             ),
    )
)
def test_parse_output(inp, expected):
    out_tbls = Radex.parse_output(inp)
    out_fluxes = [tbl['FLUX'] for tbl in out_tbls]
    numpy.testing.assert_array_almost_equal(out_fluxes, expected)
