from setuptools import setup, find_packages
from glob import glob

setup(
    name='pyndes',
    version='1.1',
    packages=find_packages(),
    url='https://gitlab.com/ANDES-dev/pyndes',
    license='MIT',
    author='Grigorii V. Smirnov-Pinchukov',
    author_email='smirnov@mpia.de',
    description='This package is a collection of routines created by author for his PhD',
    install_requires=[
        'astropy>=3.1',
        'autograd>=1.2',
        'scipy>=1.1.0',
        'numpy>=1.15',
        'matplotlib>=3',
        'astroquery>=0.3',
        'tqdm',
        'setuptools',
        'psutil',
        # 'hypothesis',
        'pytest>=5',
        'pytest-cov',
    ],
    scripts=glob('pyndes/scripts/*')
)
